/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notifirev2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author WIN 7
 */
public class NotifireV2 extends Application {

   @Override
    public void start(Stage primaryStage) throws IOException, ClassNotFoundException {
        Database db = new Database("db/"); 
//        AllUser a = new AllUser();
//        for(int i =  0; i < 10000; i++){
//                Random rand = new Random();
//                
//                int  n = rand.nextInt(10000) + 1;
//                Student s = new Student(n+1,Integer.toString(n),Integer.toString(n-1),"@"+Integer.toString(n/2)); 
//                
//                s.getCourseList().addList(n%50);
//                s.getCourseList().addList(n%21);
//                s.getCourseList().addList(n%77);
//                
//                a.addUser(s);
//            }
//            db.store(a,"userList");
//
         
        //s.printAll();
        
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                AllUser a = null;
                try {
                    a = (AllUser)db.load("userList");
                } catch (IOException ex) {
                    Logger.getLogger(NotifireV2.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(NotifireV2.class.getName()).log(Level.SEVERE, null, ex);
                }
                Student s = (Student)a.getUser(1558);
                s.printAll();
                System.out.println("x");
            }
        });
        
        
        StackPane root = new StackPane();
        root.getChildren().add(btn);
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        launch(args);
    }
    
}
