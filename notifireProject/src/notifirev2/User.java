
package notifirev2;
import java.io.Serializable;
import java.util.*;


public abstract class User implements Serializable  {
    private static final long serialVersionUID = 1L;
    private int id;
    private String password;
    private String name;
    private String email;
    private CourseList courseList;
    public User(int id,String password,String name, String email){
        this.id =id;
        this.password = password;
        this.email = email;
        this.name = name;
        this.courseList = new CourseList(this.id);
    }
    
    @Override
    public String toString(){
        return Integer.toString(id);
    }
    
    public boolean checkPassword(String passwordInput){
        return passwordInput.equals(this.password);
    }
    public boolean changePassword(String oldPassword, String newPassword){
        if(checkPassword(oldPassword)){
            this.password=newPassword;
            return true;
        }
        return false;
    }
    public void addCourse(CourseList id){
        this.courseList = id; 
    }

    public int getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public boolean setEmail(String password, String email) {
         if(checkPassword(password)){
            this.email = email;
            return true;
        }
        return false;
    }

    public CourseList getCourseList() {
        return courseList;
    }
     public void printAll(){
         System.out.println(this.id);
         System.out.println(this.password);
         System.out.println(this.name);
         System.out.println(this.courseList);
         System.out.println(this.courseList.getList());
         System.out.println(this.email);
     }
    
}
