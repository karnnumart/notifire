
package notifirev2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.*;
import java.util.*;

public class Database {
    private File file;
    private String dir ;
    
    public Database(String dir) throws IOException{
        this.dir = dir;
        this.file = new File(dir);
        file.setReadOnly();
        Files.setAttribute(file.toPath(), "dos:hidden", true, LinkOption.NOFOLLOW_LINKS);
    }
    public boolean exist(String fName)throws FileNotFoundException, IOException{
        String dest = dir+fName;
        return new File(dest).exists();
    }
    public boolean store(Object obj,String fName) throws FileNotFoundException, IOException{
        String dest = dir+fName;
        try{
            new FileInputStream(dest);
            System.out.println("Already Exist");
            return false;
            }
        catch(FileNotFoundException e){
            File tmp = new File(dest);
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(dest));
            oos.writeObject(obj);
            Files.setAttribute(tmp.toPath(), "dos:hidden", true, LinkOption.NOFOLLOW_LINKS);
            tmp.setReadOnly();
            System.out.println("Saved");
            return true;
            }
    }
    public Object load(String fName) throws FileNotFoundException, IOException, ClassNotFoundException{
        String dest = dir+fName;
        try{
            ObjectInputStream  ois = new ObjectInputStream(new FileInputStream(dest));
            return ois.readObject();
            
        }
        catch(IOException e){
            System.out.println("File not found");
            return null;
        }
    }
        
}
