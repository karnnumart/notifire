
package notifirev2;

import java.io.Serializable;
import java.util.*;



public class AllUser implements  Serializable {
    private HashMap<Integer,User> map;
    
    public AllUser() {
       map=new HashMap<>();  
    }
    
    public User getUser(int id) {
        return map.get(id);
    }

    public boolean addUser(User user) {
        if(map.containsValue(user)){
            System.out.println("already Exist");
            return false;
        }
        map.put(user.getID(), user);
        System.out.println("complete "+user);
        return true;
    }
    
}
