
package notifirev2;


// A Java program for a Server 
import java.net.*; 
import java.io.*; 
  
public class Server 
{ 
    //initialize socket and input stream 
    private Socket          socket   = null; 
    private ServerSocket    server   = null; 
    private DataInputStream in       =  null; 
  
    // constructor with port 
    public Server(int port) 
    { 
        // starts server and waits for a connection 
        try
        { 
            server = new ServerSocket(port); 
            System.out.println("Server started"); 
  
            System.out.println("Waiting for a client ..."); 
  
            socket = server.accept(); 
            System.out.println("Client accepted"); 
  
            // takes input from the client socket 
            in = new DataInputStream( new BufferedInputStream( socket.getInputStream() ) ); 
            
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            ObjectOutputStream oos = new ObjectOutputStream(dos);
  
            Student st = new Student(575575575,"Kuay","ManU-KaK","SaveMourinho");
            try{
                oos.writeObject(st);
            }
            catch(IOException i){ 
                    System.out.println(i); 
            } 
            // reads message from client until "Over" is sent 
//            while (!line.equals("Over")) 
//            { 
//                try
//                { 
//                    line = in.readUTF(); 
//                    System.out.println(line); 
//  
//                } 
//                catch(IOException i) 
//                { 
//                    System.out.println(i); 
//                } 
//            } 
            System.out.println("Closing connection"); 
  
            // close connection 
            socket.close(); 
            in.close(); 
        } 
        catch(IOException i) 
        { 
            System.out.println(i); 
        } 
    } 

} 
