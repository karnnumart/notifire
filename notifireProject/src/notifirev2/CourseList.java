
package notifirev2;
import java.io.Serializable;
import java.util.ArrayList;


public class CourseList implements Serializable  {
    private int ID;
    private ArrayList list;

    public CourseList(int ID) {
        this.ID = ID;
        this.list = new ArrayList();
    }
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public ArrayList getList() {
        return list;
    }

    public void addList(Object c) {
        this.list.add(c);
    }
    
}
